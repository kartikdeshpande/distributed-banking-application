import glob
import sys
import os
import hashlib
import logging
import socket
import shutil
import random
import SocketServer
from struct import unpack
from struct import pack
from threading import Thread
import time
from threading import Lock

from socket_pool import socket_pool
from chandy_lamport import chandy_lamport

logging.basicConfig(level=logging.DEBUG)

# Protobuf module for marshalling and unmarshalling messages
import bank_pb2


class bank:
	initialization_status = False
	money_transfer_status = False
	Account_Balance = 0
	all_branches = []
	critical_section_lock = Lock()
	
	##############################
	# Initialize objects
	def initialize( self,socket_pool_param, Marker_param):
		SOCKET_POOL = socket_pool_param
		CHANDY_MARKER = Marker_param
		initialization_status = True


        ##############################
        # Controller asked me to initialize branch with an ammount
        def initBranch (self, balance_param, branch_list):

		self.all_branches = branch_list

              	self.all_branches.remove(BRANCH.BRANCH_NAME)

                self.Account_Balance = balance_param
                print "Branch Initialized ---"

                # start sending money to other branches ( as a daemon )
                thread = Thread(target = self.TransferMoney)
                thread.daemon = True
                thread.start()


	############################
        # Running as a daemon to transfer money to other branches at different intrevals
        def TransferMoney(self):
        	while 1:
        		if self.money_transfer_status == True:

                                RandomBranch = random.choice(self.all_branches)

                               	NextIp = RandomBranch.ip
                            	NextPort = RandomBranch.port
                            	NextBranch = RandomBranch.name

                             	amount = (int) ((random.randint(1,4) * self.Account_Balance) /100)

                           	transfer_money_msg = bank_pb2.Transfer()
                              	transfer_money_msg.money = int(amount)
                            	pb_msg = bank_pb2.BranchMessage()
                            	pb_msg.transfer.CopyFrom(transfer_money_msg)
                           	encoded = pb_msg.SerializeToString()

                           	server_response = SOCKET_POOL.send_message_Protobuf(NextIp, NextPort, encoded)

                         	# Critical section, self.Account_Balance accessed by multiple threads
                         	with self.critical_section_lock:
                        		self.Account_Balance = self.Account_Balance - amount
                             	time.sleep(random.randint(1,5))
	
        ################################
        # Receive money from other branches
        def ReceiveMoney ( self, receivedmoney, ReceivingBranch ):

                if len(CHANDY_MARKER.snapshot_history_list) > 0 :
                        snapshot_num = CHANDY_MARKER.snapshot_history_list[-1]

                        # check if incomming message needs to be recorded
                        if CHANDY_MARKER.MARKER_MESSAGE_CHANNEL_STATE[ snapshot_num , ReceivingBranch ][0] == True and CHANDY_MARKER.MARKER_MESSAGE_BALANCE != 0:
                                amount = int(CHANDY_MARKER.MARKER_MESSAGE_CHANNEL_STATE[ snapshot_num , ReceivingBranch ][1]) + receivedmoney
                                CHANDY_MARKER.MARKER_MESSAGE_CHANNEL_STATE[snapshot_num , ReceivingBranch ] = (True , amount )

                #print "-----------------------------------------------"
                #print "Recorded Balance " + str(MARKER_MESSAGE_self.Account_Balance)
                #print self.MARKER_MESSAGE_CHANNEL_STATE
                #print "Now balance " + str(self.Account_Balance) + " status " + str(money_transfer_status)
                #print "-----------------------------------------------"

                # Critical section, self.Account_Balance accessed by multiple threads
                with self.critical_section_lock:
                        self.Account_Balance = self.Account_Balance + receivedmoney
